FROM ubuntu:20.04

RUN apt-get update && apt-get install wget curl apt-transport-https gnupg rsync docker.io git -y

RUN curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

RUN chmod +x /usr/local/bin/docker-compose

RUN rm -rf /var/lib/apt/lists/*

# Add a new user "gitlab-runner" with user id 1001
RUN useradd -m -u 1001 gitlab-runner

# user assumed to have same uid as user host, useful for local build-jobs inside container
RUN useradd -m -u 1000 local-user

# Change to non-root privilege
USER gitlab-runner

WORKDIR /opt/workspace
